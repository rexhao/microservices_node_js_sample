## MongoDB / NodeJS Express / Docker Bootstrap

This is a MongoDB/Express/Docker Bootstrap file to speed up the development for your microservice(s).

### Assumptions

By using this stack is assuming that you are master in entire modern back-end stack: Node, Express, MongoDB

---

## Start Project with NPM

### Assumptions

- You have Node and MongoDB installed
- You want to use NodeJS for local development.

To initialize the project

```bash
$ npm install
```

Duplicate .env-example to .env and fill in necessary details for development.
**PS: Remember to put in MongoDB cretentials into .env file as well as uncomment dotenv library files in server.js and test units.**

Start the server on production simulation.

```bash
$ npm run start
```

Start the server on development with Nodemon.

```bash
$ npm run start:dev
```

Run test units

```bash
$ npm run test
```

Access and Port Number

Api will be available on this path : http://localhost:3000

Mongodb will be solely dependent on your machine.

---

## Start with Docker

### Assumptions

- You have Docker and Docker-Compose installed (Docker for Mac, Docker for Windows, get.docker.com and manual Compose installed for Linux).
- You want to use Docker for local development and have dev and prod Docker images be as close as possible.
- You don't want to lose fidelity in your dev workflow. You want a easy environment setup, using local editors, node debug/inspect, local code repo, while node server runs in a container.
- You use `docker-compose` for local development only (docker-compose was never intended to be a production deployment tool anyway).

To initialize the project

```bash
$ docker-compose up -d --build
```

For eslint airbnb config and beautify config for vscode

```bash
$ npm i eslint eslint-config-airbnb eslint-config-prettier eslint-plugin-import eslint-plugin-jsx-a11y eslint-plugin-node eslint-plugin-prettier eslint-plugin-react prettier
```

Install a package while docker-compose was running your app (`--save` will add it to the package.json for next `docker-compose build`).

```bash
$ docker-compose exec -w /user/app/user-api node npm install --save <package name>
```

Run test units

```bash
$ docker exec -it <container_id> npm run test
```

Commands below will help to access into Docker container

```bash
$ docker exec -it <container_id> bash
```

Access and Port Number

Api will be available on this path : http://localhost:3000

Mongodb will be available on details below by default.

```
host: localhost
port: 27017
username: api-user
password: api-password
```
