const AppError = require('./../utils/appError');

/**
 * Handler Cast Error from JWT token
 * @param {Error} err
 */
const handleJWTError = () =>
  new AppError('Invalid Token. Please login again', 401);

const handleJWTExpiredError = () =>
  new AppError('Your Token has expired, please login again', 401);
/**
 * Handler Cast Error from DATABASE
 * @param {Error} err
 */
const handleCastErrorDB = err => {
  const message = `Invalid ${err.path} : ${err.value}.`;
  return new AppError(message, 400);
};

/**
 * Handler Duplication Error
 * @param {Error} err
 */
const handleDuplicateFieldDB = err => {
  const value = err.errmsg.match(/(["'])(\\?.)*?\1/)[0];
  const message = `Duplicate field value ${value}, Please use another value`;
  return new AppError(message, 400);
};

/**
 * Handler Validation Error
 * @param {*} err
 */
const handleValidationErrorDB = err => {
  const errors = Object.values(err.errors).map(el => el.message);
  const message = `Invalid input data. ${errors.join('. ')}`;
  return new AppError(message, 400);
};

/**
 * Error handler for Development
 * @param {*} err
 * @param {*} res
 */
const sendErrorDev = (err, req, res) => {
  // A) API
  return res.status(err.statusCode).json({
    status: err.status,
    error: err,
    message: err.message,
    stack: err.stack
  });
};

/**
 * Erorr handler from Production
 * @param {*} err
 * @param {*} res
 */
const sendErrorProd = (err, req, res) => {
  /* API */
  // A) Operational, trusted Error
  if (err.isOperational) {
    return res.status(err.statusCode).json({
      status: err.status,
      message: err.message
    });
  }

  // Programming or other unknown error which do not leak details to the client
  // 1) Log error
  console.log('Error : ', err);
  // 2) send generic error
  return res.status(500).json({
    status: 'error',
    message: 'Something went wrong!'
  });
};

module.exports = (err, req, res, next) => {
  err.statusCode = err.statusCode || 500;
  err.status = err.status || 'error';

  if (
    process.env.NODE_ENV === 'development' ||
    process.env.NODE_ENV === 'test'
  ) {
    sendErrorDev(err, req, res);
  } else if (process.env.NODE_ENV === 'production') {
    let error = { ...err };
    // references are not taken
    error.message = err.message;

    // if is DB error
    if (error.name === 'CastError') error = handleCastErrorDB(error);
    if (error.code === 11000) error = handleDuplicateFieldDB(error);
    if (error.name === 'ValidationError')
      error = handleValidationErrorDB(error);

    // if jwt token is error
    if (error.name === 'JsonWebTokenError') error = handleJWTError();
    if (error.name === 'TokenExpiredError') error = handleJWTExpiredError();

    sendErrorProd(error, req, res);
  }
};
