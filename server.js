// UNCOMMENT THIS IF YOU ARE USING NPM RUN SERVE
// const dotenv = require('dotenv');
const { connectDatabase } = require('./utils/database');

// handler uncaughtException crash
process.on('uncaughtException', err => {
  console.log('UNHANDLER REJECTION! Shutting down...');
  console.log(err);
  process.exit(1);
});

// UNCOMMENT THIS IF YOU ARE USING NPM RUN SERVE
// dotenv.config({ path: './.env' });

// connect db
connectDatabase()
  .then(() => console.log('Database connection establish successful.'))
  .catch(e =>
    console.log(`Something weng wrong on connecting database, Error: ${e}`)
  );

// initialize app
const app = require('./app');

// startserver
const port = process.env.PORT || 3000;
const server = app.listen(port, () => {
  console.log(`App Running on port ${port}...`);
});

// handler unexpected rejection
process.on('unhandledRejection', err => {
  console.log(err);
  console.log('UNHANDLER REJECTION! Shutting down...');
  // gracefully close the application
  server.close(() => {
    process.exit(1);
  });
});

// handling daily system reboot from Heroku
process.on('SIGTERM', () => {
  console.log('---SIGTERM RECEIVED, SHUTTING DOWN GRACEFULLY!---');
  server.close(() => {
    console.log('---PROCESS TERMINATED---');
  });
});

module.exports = app;
