const faker = require('faker');

/**
 * Factory for creating random user
 */
exports.standardUser = () => {
  // for password
  const password = `${faker.internet.password}`;

  // return object
  return {
    name: `${faker.name.firstName()} ${faker.name.lastName()}`,
    email: (`${faker.internet.email()}`).toLowerCase(),
    password: password,
    passwordConfirm: password
  };
}
