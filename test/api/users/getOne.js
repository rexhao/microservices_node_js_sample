/* eslint-disable */
// UNCOMMENT THIS IF YOU ARE USING NPM RUN SERVE
// const dotenv = require('dotenv');
// dotenv.config({ path: './.env' });
const { expect } = require('chai');
const request = require('supertest');
const mongoose = require('mongoose');

// get environment file done
const app = require('../../../app');
const {
  connectDatabase,
  disconnectDatabase
} = require('../../../utils/database');

// User
const User = require('../../../models/userModels');

// faker seeder
const { standardUser } = require('../../seeder/user.js');

// Test user;
describe('Test Auth User Api', function() {
  // before test start
  before(done => {
    connectDatabase()
      .then(() => done())
      .catch(err => console.log(`Database connection error occured, ${err}`));
  });

  // after test done
  after(() => {
    disconnectDatabase();
  });

  describe('/Sign Up User', function() {
    // remove all User data
    beforeEach(done => {
      User.deleteMany()
        .then(() => done())
        .catch(e =>
          console.log(`Something went wrong, please check again ${e}`)
        );
    });

    // register user
    it('should register one user', async () => {
      // generate data
      const data = standardUser();

      // get result from database
      const res = await request(app)
        .post(`/api/v1/users/signup`)
        .send(data);

      // verify if values are exactly the same
      expect(res.statusCode).to.equal(201);
      expect(res.body.data).to.be.an('object');
      expect(res.body.data.user).to.be.an('object');
      expect(res.body.data.user.photo).to.equal('default.jpg');
      expect(res.body.data.user.role).to.equal('user');
      expect(res.body.data.user.name).to.equal(data.name);
      expect(res.body.data.user.email).to.equal(data.email);
      expect(res.body.status).to.equal('success');
      expect(res.body.token).to.be.an('string');
    });

    // register user
    it('should always register user as user type', async () => {
      // generate data
      var data = standardUser();
      data.role = 'admin';

      // get result from database
      const res = await request(app)
        .post(`/api/v1/users/signup`)
        .send(data);

      // verify if values are exactly the same
      expect(res.statusCode).to.equal(201);
      expect(res.body.data.user.role).to.equal('user');
      expect(res.body.data.user.role).to.not.equal('admin');
    });
  });
});
