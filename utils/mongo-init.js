// Login with authentication
db.auth('web-admin', 'web-password')

// Create Database Layer
db = db.getSiblingDB('service-users')

// Create User to use for api user
db.createUser(
  {
    user: "api-user",
    pwd: "api-password",
    roles: [
      {
        role: "readWrite",
        db: "service-users"
      }
    ]
  }
);
