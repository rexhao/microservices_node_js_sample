const mongoose = require('mongoose');

// connect actual db
const databaseCredentails = () => {
  // connect database
  return process.env.DATABASE.replace(
    '<PASSWORD>',
    process.env.DATABASE_PASSWORD
  );
};

// export function
exports.connectDatabase = async () => {
  const DB = databaseCredentails();

  // return promise
  await mongoose.connect(DB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  });
};

// disconnect connection
exports.disconnectDatabase = () => {
  return mongoose.disconnect();
};
